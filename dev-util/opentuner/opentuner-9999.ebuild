# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI=8

PYTHON_COMPAT=(
 python2_7
 python3_{8..11}
)
# pypy3

inherit git-r3 distutils-r1

DESCRIPTION="framework for building domain-specific multi-objective program autotuners"
HOMEPAGE="http://opentuner.org/"
SRC_URI=""

EGIT_REPO_URI="https://github.com/jansel/opentuner.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
 >=sci-visualization/gnuplot-4.0
 >=dev-python/matplotlib-2.0.0[${PYTHON_USEDEP}]
 >=dev-python/sqlalchemy-0.8.2[${PYTHON_USEDEP}]
"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	local DOCS=( README.md )
	distutils-r1_python_install_all
}

