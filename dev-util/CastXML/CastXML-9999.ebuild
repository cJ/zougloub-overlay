EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/CastXML/CastXML"
DESCRIPTION="A C-family abstract syntax tree XML output tool."
HOMEPAGE="https://github.com/CastXML/CastXML"
LICENSE="Apache-2.0"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	)
	cmake_src_configure
}
