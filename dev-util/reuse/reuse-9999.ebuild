# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI=8
DISTUTILS_USE_PEP517=poetry

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Tool for helping compliance w/ REUSE Initiative recommendations"
HOMEPAGE="https://reuse.software"
SRC_URI=""

EGIT_REPO_URI="https://github.com/fsfe/reuse-tool"

# https://git.fsfe.org/reuse/tool

LICENSE="GPL-3 Apache-2.0 CC-BY-SA-4.0 CC0-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
"
RDEPEND="
 >=dev-python/binaryornot-0.4.4[${PYTHON_USEDEP}]
 >=dev-python/boolean-py-3.8[${PYTHON_USEDEP}]
 >=dev-python/license-expression-1.2[${PYTHON_USEDEP}]
 >=dev-python/jinja-2.11.2[${PYTHON_USEDEP}]
 >=dev-python/python-debian-0.1.38[${PYTHON_USEDEP}]
 >=dev-python/requests-2.25.1[${PYTHON_USEDEP}]
"

