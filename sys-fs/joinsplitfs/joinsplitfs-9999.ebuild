# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit subversion autotools

DESCRIPTION="Two virtual FUSE filesystems to join or split files."
HOMEPAGE="https://joinsplitfs.sourceforge.net/"
LICENSE="GPL-3"

RDEPEND="
 >=sys-fs/fuse-2.7.1
"
DEPEND="
 ${RDEPEND}
"
BDEPEND="virtual/pkgconfig"

KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

ESVN_REPO_URI="https://svn.code.sf.net/p/${PN}/code/trunk"
S="$WORKDIR/$P"

src_prepare() {
	mv src/* .
	eautoreconf
	default
}
