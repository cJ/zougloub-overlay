EAPI="8"



DESCRIPTION="Enables tcp keepalive features in dynamic executables using LD_PRELOAD"
HOMEPAGE="https://libkeepalive.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

src_install() {
	dolib libkeepalive.so
}
