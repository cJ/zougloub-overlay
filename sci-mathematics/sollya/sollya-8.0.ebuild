# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

inherit autotools

DESCRIPTION="A tool environment and a library for safe floating-point code development."
HOMEPAGE="https://www.sollya.org/"
SRC_URI="https://www.sollya.org/releases/${P}/${P}.tar.gz"

LICENSE="CeCILL-C"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
 dev-libs/mpfr
 dev-libs/libxml2
 sci-libs/mpfi
 sci-libs/fplll
"
RDEPEND="${DEPEND}"
