# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6

inherit toolchain-funcs git-r3 autotools

DESCRIPTION="Linux-PAM login module allows a X.509 certificate based user login"
HOMEPAGE="https://github.com/OpenSC/pam_pkcs11"
SRC_URI=""
EGIT_REPO_URI="https://github.com/OpenSC/pam_pkcs11"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="curl nss +pcsc doc ldap"

# collision of /usr/bin/ts
RDEPEND="
 curl? ( net-misc/curl )
 nss? ( dev-libs/nss )
 pcsc? ( sys-apps/pcsc-lite )
 doc? ( dev-libs/libxslt )
 ldap? ( net-nds/openldap )
"
# dev-libs/libp11 ?

DEPEND="$RDEPEND"

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	econf \
	 --libdir=/$(get_libdir) \
	 $(use_with curl)
}
src_install() {
	default
	#mv "${D}/usr/"
}