# SPDX-FileCopyrightText: 2009-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

inherit font

DESCRIPTION="Flexible system of fonts designed specifically for code by David Jonathan Ross"
HOMEPAGE="https://input.fontbureau.com"
SRC_URI="https://input.djr.com/build/?fontSelection=whole&a=0&g=0&i=0&l=0&zero=0&asterisk=0&braces=0&preset=default&line-height=1.2&accept=I+do&email= -> ${P}.zip"
LICENSE="fontbureau-input"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~x86"
IUSE=""



S="${WORKDIR}/Input_Fonts"

# Only installs fonts
RESTRICT="strip binchecks primaryuri"

src_install() {
	for dir in */*; do
		FONT_SUFFIX="ttf"
		FONT_S="${dir}"
		FONTDIR="/usr/share/fonts/input"
		font_src_install
	done
}
