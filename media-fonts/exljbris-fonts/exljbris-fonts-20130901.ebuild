# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit font

DESCRIPTION="Free fonts from the exljbris Font Foundry"
HOMEPAGE="https://www.exljbris.com/"
SRC_URI="
 https://www.exljbris.com/dl/delicious-123.zip
 https://www.exljbris.com/dl/Diavlo_II_37b2.zip
 https://www.exljbris.com/dl/fontin_pc.zip
 https://www.exljbris.com/dl/FontinSans_49.zip
 https://www.exljbris.com/dl/FontinSans_Cyrillic_46b.zip
 https://www.exljbris.com/dl/tallys_15b2.zip
"

LICENSE="exljbris-eula"
SLOT="0"
KEYWORDS="~alpha amd64 arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~x86"
IUSE=""

S="${WORKDIR}"

FONT_SUFFIX="otf"
FONT_S="${S}"
FONTDIR="/usr/share/fonts/exljbris"


# Only installs fonts
RESTRICT="strip binchecks primaryuri"
