EAPI="8"

inherit cmake git-r3 toolchain-funcs multilib-minimal

DESCRIPTION="The Lime Suite application software provides drivers and SDR application support for the LMS7002M RFIC, and hardware like the LimeSDR, NovenaRF7, and others."
HOMEPAGE="https://myriadrf.org/projects/lime-suite"
SRC_URI=""
EGIT_REPO_URI="https://github.com/myriadrf/LimeSuite.git"

LICENSE="Apache"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
 net-wireless/soapysdr
 >=dev-libs/libusb-1.0
"
DEPEND="${RDEPEND}
"

multilib_src_configure() {
	local mycmakeargs=(
		-DINSTALL_UDEV_RULES=ON
		-DENABLE_NOVENARF7=OFF
		-DLIB_SUFFIX=64
	)
	cmake_src_configure
}

multilib_src_compile() {
	cmake_src_compile
}

multilib_src_install() {
	DOCS="README.md" cmake_src_install
	mv ${D}/usr/{lib,$(get_abi_LIBDIR)}
}
