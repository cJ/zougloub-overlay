EAPI="8"

inherit cmake git-r3

DESCRIPTION="OpenThread Daemon - OpenThread as a service via POSIX build mode."
HOMEPAGE="https://github.com/openthread/openthread.git"
SRC_URI=""
EGIT_REPO_URI="https://github.com/openthread/openthread.git"

LICENSE="( BSD Apache-2.0 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
 sys-libs/readline:0=
"
DEPEND="${RDEPEND}
"
src_configure() {
	# work around static mbedtls linkage into dynamic library by building everything with fPIC
	#CFLAGS+=" -fPIC"
	#CXXFLAGS+=" -fPIC"
	local mycmakeargs=(
	 -DBUILD_SHARED_LIBS=OFF # unless we add -fPIC 
	 -DOT_DAEMON=ON
	 -DOT_SPINEL_RESET_CONNECTION=ON
	 #-DOT_THREAD_VERSION=1.2
	 -DOT_COVERAGE=OFF
	 -DOT_PLATFORM=posix
	 -DOT_LOG_OUTPUT=PLATFORM_DEFINED
	 -DOT_POSIX_MAX_POWER_TABLE=ON
	 -DOT_IP6_FRAGM=ON
	 -DOT_SLAAC=ON -DOT_ANYCAST_LOCATOR=ON -DOT_BORDER_AGENT=ON -DOT_BORDER_ROUTER=ON -DOT_COAP=ON -DOT_COAP_BLOCK=ON -DOT_COAP_OBSERVE=ON -DOT_COAPS=ON -DOT_COMMISSIONER=ON -DOT_CHANNEL_MANAGER=ON -DOT_CHANNEL_MONITOR=ON -DOT_CHILD_SUPERVISION=ON -DOT_DATASET_UPDATER=ON -DOT_DHCP6_CLIENT=ON -DOT_DHCP6_SERVER=ON -DOT_DIAGNOSTIC=ON -DOT_DNS_CLIENT=ON -DOT_ECDSA=ON -DOT_HISTORY_TRACKER=ON -DOT_IP6_FRAGM=ON -DOT_JAM_DETECTION=ON -DOT_JOINER=ON -DOT_LEGACY=ON -DOT_MAC_FILTER=ON -DOT_MTD_NETDIAG=ON -DOT_NEIGHBOR_DISCOVERY_AGENT=ON -DOT_NETDATA_PUBLISHER=ON -DOT_PING_SENDER=ON -DOT_REFERENCE_DEVICE=ON -DOT_SERVICE=ON -DOT_SNTP_CLIENT=ON -DOT_SRP_CLIENT=ON -DOT_LOG_LEVEL_DYNAMIC=ON -DOT_COMPILE_WARNING_AS_ERROR=ON -DOT_RCP_RESTORATION_MAX_COUNT=2 -DOT_UPTIME=ON
	)
	cmake_src_configure
}

src_install() {
	DOCS="README.md" cmake_src_install

	# work around missing installations
	#dolib.so src/cli/libopenthread-cli-ftd.so
	#dolib.so src/core/libopenthread-ftd.so
	#dolib.so src/posix/platform/libopenthread-posix.so
	#dolib.so src/lib/hdlc/libopenthread-hdlc.so
	#dolib.so src/lib/spinel/libopenthread-spinel-rcp.so
	#dolib.so src/lib/platform/libopenthread-platform.so
}
