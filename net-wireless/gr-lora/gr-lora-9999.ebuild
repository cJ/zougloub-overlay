# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit cmake python-single-r1

DESCRIPTION="GNU Radio blocks for receiving LoRa modulated radio messages"
HOMEPAGE="https://github.com/rpp0/gr-lora"

KEYWORDS="~x86 ~amd64"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/rpp0/gr-lora.git"
fi
LICENSE="GPL-3"
SLOT="0/${PV}"
IUSE=""

#xtrx? ( net-wireless/libxtrx )
RDEPEND="${PYTHON_DEPS}
	dev-libs/boost:=
	>=net-libs/liquid-dsp-1.3.0
	"
DEPEND="${RDEPEND}"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

src_configure() {
	local mycmakeargs=(
	 -DENABLE_DEFAULT=OFF
	 -DPYTHON_EXECUTABLE="${PYTHON}"
	 -DGRLORA_DEBUG=ON
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install
	python_optimize
	mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${P}"
}
