# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit cmake python-single-r1

DESCRIPTION="GNU Radio blocks for LimeSDR"
HOMEPAGE="https://github.com/myriadrf/gr-limesdr"


KEYWORDS="~x86 ~amd64"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/myriadrf/gr-limesdr"
	EGIT_BRANCH="gr-3.8"
	EGIT_COMMIT="bf6bcf4aa5a2109d39a4d382e51f0c80c267299d"
fi
LICENSE="GPL-3"
SLOT="0/${PV}"
IUSE=""

#xtrx? ( net-wireless/libxtrx )
RDEPEND="${PYTHON_DEPS}
	dev-libs/boost:=
	"
DEPEND="${RDEPEND}"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

src_configure() {
	local mycmakeargs=(
	 -DENABLE_DEFAULT=OFF
	 -DPYTHON_EXECUTABLE="${PYTHON}"
	 -DGRLORA_DEBUG=ON
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install
	python_optimize
	mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${P}"
}
