# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit cmake python-single-r1

DESCRIPTION="GNU Radio blocks for receiving RDS FM Radio"
HOMEPAGE="https://github.com/bastibl/gr-rds"

KEYWORDS="~x86 ~amd64"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/bastibl/gr-rds"
fi
LICENSE="GPL-3"
SLOT="0/${PV}"
IUSE=""

#xtrx? ( net-wireless/libxtrx )
RDEPEND="${PYTHON_DEPS}
	dev-libs/boost:=
	>=net-libs/liquid-dsp-1.3.0
	"
DEPEND="${RDEPEND}"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

src_configure() {
	local mycmakeargs=(
	 -DENABLE_DEFAULT=OFF
	 -DPYTHON_EXECUTABLE="${PYTHON}"
	 -DGRLORA_DEBUG=ON
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install
	python_optimize
	mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${P}"
}
