# SPDX-FileCopyrightText: 2020-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
)

inherit git-r3 bash-completion-r1 distutils-r1

DESCRIPTION="Double-Entry Accounting from Text Files"
HOMEPAGE="https://furius.ca/beancount/"
SRC_URI=""

EGIT_REPO_URI="https://github.com/beancount/beancount"
EGIT_BRANCH="v2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86"
IUSE="doc test"

RDEPEND="
 >=dev-python/python-dateutil-2.6.0[${PYTHON_USEDEP}]
 >=dev-python/ply-3.0[${PYTHON_USEDEP}]
 >=dev-python/click-7.0[${PYTHON_USEDEP}]
"

DEPEND="${RDEPEND}
"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_compile_all() {
	:
}

python_test() {
	:
}

python_install_all() {
	distutils-r1_python_install_all
}

