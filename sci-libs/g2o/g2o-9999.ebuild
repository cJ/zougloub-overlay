# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit cmake git-r3

DESCRIPTION="Intermediate Computational Graph Representation for Deep Learning Systems"
HOMEPAGE="https://github.com/RainerKuemmerle/g2o"
EGIT_REPO_URI="https://github.com/RainerKuemmerle/g2o"
EGIT_SUBMODULES=()

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# TODO: fusion/cuda submodule plugin
DEPEND="
"
RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
	 -DBUILD_SHARED_LIBS=ON
	 #-Dg2o_LIBRARY_OUTPUT_DIRECTORY:PATH="${D}/usr/lib64"
	 #-Dg2o_RUNTIME_OUTPUT_DIRECTORY:PATH="${D}/usr/bin"
	)

	cmake_src_configure
}

