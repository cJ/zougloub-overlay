EAPI="8"
DESCRIPTION="Library for reading and writing matlab .MAT files"
HOMEPAGE="http://sourceforge.net/projects/matio"
EGIT_REPO_URI="https://github.com/rdiankov/openrave.git"
LICENSE="LGPL-2.1"
KEYWORDS="~x86 ~amd64"
SLOT=0
IUSE="+doc +examples"

inherit git-r3 cmake

RDEPEND="
"

DEPEND="
 $RDEPEND
"

src_prepare() {
	:
}

pkg_setup() {
	:#use fortran && fortran_pkg_setup
}

src_configure() {
	cmake_configure \
	 || die "econf failed"
}

src_test() {
	:
}

src_install() {
	cmake_install
}
