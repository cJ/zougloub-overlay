# Copyright 2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit multiprocessing distutils-r1 git-r3 cmake

DESCRIPTION="Library for Machine Intelligence"

HOMEPAGE="https://www.tensorflow.org/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/tensorflow/tensorflow"
#EGIT_COMMIT="v${PV//_}"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~ppc ~ppc64 ~sparc ~x86 ~x86-fbsd ~x86-freebsd ~amd64-linux ~x86-linux ~x86-macos"
IUSE=""
RESTRICT="primaryuri"

RDEPEND="
"

DEPEND="
 >=dev-python/wheel-0.26.0
 >=dev-python/six-1.10.0
 >=dev-lang/swig-3.0.8
"

src_prepare() {
	sed -i -e 's/protobuf == 3.0.0a3/protobuf >= 2.6.0/g' \
	 tensorflow/tools/pip_package/setup.py
}

CMAKE_USE_DIR="${S}/${PN}/contrib/cmake"


src_configure() {
	local mycmakeargs=(
	 -Dtensorflow_BUILD_SHARED_LIB:BOOL="1"
	)
	cmake_src_configure
}
