EAPI="8"
DESCRIPTION="FFTS FFT library"
HOMEPAGE="http://github.com/anthonix/ffts"
EGIT_REPO_URI="https://github.com/linkotec/ffts"
LICENSE="BSD"
KEYWORDS="~x86"
SLOT=0
IUSE="+single +sse -neon"

inherit flag-o-matic git-r3 cmake

RDEPEND="
"

DEPEND="
 $RDEPEND
"

src_configure() {
	filter-flags '-O*' # ffts is super-picky about CFLAGS!
	local myeconfargs=(
	 $(use_enable single)
	 $(use_enable sse)
	 $(use_enable neon)
	)
	cmake_src_configure
}

DOCS=( README.md )
