EAPI="8"
DESCRIPTION="ndarray for gpu"
HOMEPAGE="http://deeplearning.net/software/libgpuarray/"
EGIT_REPO_URI="https://github.com/Theano/libgpuarray.git"
LICENSE="MIT"
KEYWORDS="~x86 ~amd64"
SLOT=0
IUSE=""

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit git-r3 cmake distutils-r1

# TODO CUDA OPENCL CLBLAS

RDEPEND="
 sci-libs/clblas
"

DEPEND="
 $RDEPEND
"

src_prepare() {
	cmake_src_prepare
	distutils-r1_src_prepare
}

src_configure() {
	cmake_src_configure
	distutils-r1_src_configure
}

src_compile() {
	cmake_src_compile
	distutils-r1_src_compile
}

src_install() {
	cmake_src_install
	distutils-r1_src_install
}

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}
