# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="8"

inherit toolchain-funcs autotools git-r3



DESCRIPTION="Command-line mod player using libxmp"
HOMEPAGE="http://xmp.sourceforge.net/"

EGIT_REPO_URI="https://github.com/cmatsuoka/xmp-cli.git"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RESTRICT="mirror"

DEPEND="
 media-sound/libxmp
"

RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
}

