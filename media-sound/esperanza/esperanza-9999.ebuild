# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="8"
DESCRIPTION="Esperanza - a QT4 client for xmms2."
HOMEPAGE="http://xmms2.xmms.org"

inherit toolchain-funcs  qt4-r2 git-r3

EGIT_REPO_URI="git://git.xmms2.org/xmms2/esperanza.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RESTRICT="mirror"

RDEPEND="
 >=media-sound/xmms2-0.2.8_rc2[cxx]
 dev-qt/qtcore
 dev-qt/qtgui
 dev-qt/qtxmlpatterns
"

DEPEND="
 ${RDEPEND}
"

src_prepare() {
	echo """
!win32 {
	INCLUDEPATH += /usr/include/xmms2
	LIBS += -lxmmsclient -lxmmsclient++ -lboost_signals
}
""" > conf.pri


	sed -i \
	 -e 's/XKeycodeToKeysym.appDpy, map->modifiermap.mapIndex., symIndex./XkbKeycodeToKeysym(appDpy, map->modifiermap[mapIndex], symIndex, 0)/g' \
	 -e 's@X11/Xlib.h@X11/XKBlib.h@g' \
	 src/tools/globalshortcut/globalshortcutmanager_x11.cpp
}

src_install() {
	default-src_install
	doicon data/images/esperanza.png
	make_desktop_entry ${PN} "Esperanza" ${PN} "Qt4;AudioVideo;Player"
}

