# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
DISTUTILS_USE_PEP517=setuptools

inherit distutils-r1 git-r3

EGIT_REPO_URI="https://github.com/pyopencl/pyopencl"
EGIT_SUBMODULES=( '*' )

DESCRIPTION="Python wrapper for OpenCL"
HOMEPAGE="https://mathema.tician.de/software/pyopencl/
	https://pypi.org/project/pyopencl/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="examples opengl"

DEPEND=">=virtual/opencl-2"
RDEPEND="${DEPEND}
	>=dev-python/appdirs-1.4.0[${PYTHON_USEDEP}]
	>=dev-python/mako-0.3.6[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	>=dev-python/pytools-2021.2.7[${PYTHON_USEDEP}]"
# libglvnd is only needed for the headers
BDEPEND="dev-python/numpy[${PYTHON_USEDEP}]
	>=dev-python/pybind11-2.5.0[${PYTHON_USEDEP}]
	opengl? ( media-libs/libglvnd )"

# The test suite fails if there are no OpenCL platforms available, and
# even if there is one (which requires the presence of both an OpenCL
# runtime *and* hardware supported by it - simply emerging any runtime
# is not enough) the vast majority of tests end up skipped because by
# default the portage user hasn't got sufficient privileges to talk
# to the GPU.
RESTRICT="test"

python_configure_all() {
	local myconf=()
	if use opengl; then
		myconf+=(--cl-enable-gl)
	fi

	"${EPYTHON}" configure.py \
		"${myconf[@]}"
}

python_install_all() {
	if use examples; then
		dodoc -r examples
		docompress -x /usr/share/doc/${PF}/examples
	fi

	distutils-r1_python_install_all
}
