# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Python tools for handling intervals (ranges of comparable objects)"
HOMEPAGE="https://github.com/kvesteri/intervals"
SRC_URI=""

EGIT_REPO_URI="https://github.com/kvesteri/intervals"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
 ${PYTHON_DEPS}
 dev-python/infinity[${PYTHON_USEDEP}]
"
DEPEND="
 $RDEPEND
"
BDEPEND="
"
