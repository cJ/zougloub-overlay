# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="A Python module providing a mesh-holding octree"
HOMEPAGE="https://github.com/mhogg/pyoctree"
SRC_URI=""
EGIT_REPO_URI="https://github.com/mhogg/pyoctree"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
 ${PYTHON_DEPS}
"
DEPEND="
 dev-python/numpy[${PYTHON_USEDEP}]
 ${RDEPEND}
"
BDEPEND="
 dev-python/cython[${PYTHON_USEDEP}]
"

DOCS=( README.rst )

python_prepare_all() {
	sed -i -e 's@/openmp@-fopenmp@g' setup.py || die
	sed -i -e "s@'','pyoctree'@'pyoctree'@g" setup.py || die
	sed -i -e 's/package_data/#package_data/g' setup.py || die
	#rm -rf pyoctree.egg-info
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}
