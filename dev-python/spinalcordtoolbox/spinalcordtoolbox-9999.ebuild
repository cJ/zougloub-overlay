EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3 cmake

DESCRIPTION="Toolbox for processing spinal cord MRI data"
HOMEPAGE="https://sourceforge.net/projects/spinalcordtoolbox/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/neuropoly/spinalcordtoolbox"
LICENSE="MIT"
SLOT="0"
IUSE=""
KEYWORDS="~amd64 ~x86"

RDEPEND="
 >=sci-libs/nibabel-2.0.0
 >=sci-libs/itk-4.5.1[review]
 >=sci-libs/vtk-6.0.0
 >=sci-biology/dipy-0.10[${PYTHON_USEDEP}]
 sci-biology/ants
"

DEPEND="
 $RDEPEND
"

SCT_PROGLIST="
 isct_vesselness
 isct_propseg
 sct_change_nifti_pixel_type
 isct_crop_image
 sct_detect_spinalcord
 isct_dice_coefficient
 sct_hausdorff_distance
 sct_modif_header
 isct_orientation3d
 isct_bsplineapproximator
"


src_prepare() {
	distutils-r1_src_prepare
	for program in $SCT_PROGLIST; do
		pushd dev/$program || die
			S=$PWD CMAKE_USE_DIR=$PWD BUILD_DIR=$PWD/build cmake_src_prepare
		popd
	done
}

src_configure() {
	for program in $SCT_PROGLIST; do
		pushd dev/$program || die
			S=$PWD CMAKE_USE_DIR=$PWD BUILD_DIR=$PWD/build cmake_src_configure
		popd
	done
}

src_compile() {
	distutils-r1_src_compile

	for program in $SCT_PROGLIST; do
		pushd dev/$program
			S=$PWD CMAKE_USE_DIR=$PWD BUILD_DIR=$PWD/build cmake_src_compile
		popd
	done

	#$(use threads || echo --without-threading)
}

src_test() {
	testing() {
		:
	}
	python_execute_function testing
}

src_install() {
	distutils-r1_src_install
	for program in $SCT_PROGLIST; do
		cp dev/$program/build/$program $D/$EPREFIX/usr/
	done
}

pkg_setup() {
	python-any-r1_pkg_setup
}

