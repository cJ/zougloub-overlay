EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Bindings for the PortAudio library and a few convenience functions to play and record NumPy arrays containing audio signals."
HOMEPAGE="https://github.com/spatialaudio/python-sounddevice"
SRC_URI=""
EGIT_REPO_URI="https://github.com/spatialaudio/python-sounddevice"
LICENSE="MIT"
SLOT="0"
IUSE=""
KEYWORDS="~amd64 ~x86"

RDEPEND="
 >=media-libs/portaudio-19
 >=dev-python/cffi-1.5[${PYTHON_USEDEP}]
"
DEPEND="
 $RDEPEND
"

src_prepare() {
	distutils-r1_src_prepare
}

src_compile() {
	distutils-r1_src_compile
	#$(use threads || echo --without-threading)
}

src_test() {
	testing() {
		:
	}
	python_execute_function testing
}

src_install() {
	distutils-r1_src_install
}

pkg_setup() {
	python-any-r1_pkg_setup
}

