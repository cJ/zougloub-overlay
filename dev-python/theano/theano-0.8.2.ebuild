# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
) )

inherit distutils-r1 versionator

MYPN=Theano
MYP=${MYPN}-$(replace_version_separator 3 '')

DESCRIPTION="Define and optimize multi-dimensional arrays mathematical expressions"
HOMEPAGE="https://github.com/Theano/Theano"

SLOT="0"
LICENSE="BSD"
IUSE="test"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

RDEPEND="
	>=dev-python/numpy-1.6.2[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	>=sci-libs/scipy-0.11[${PYTHON_USEDEP}]
"
DEPEND="${RDEPEND}
	test? ( dev-python/nose[${PYTHON_USEDEP}] )"

S="${WORKDIR}/${MYP}"

python_prepare_all() {
	find -type f -name "*.py" -exec \
	sed \
		-e 's:theano.compat.six:six:g' \
		-i '{}' + || die

	rm ${PN}/compat/six.py || die

	distutils-r1_python_prepare_all
}

python_test() {
	nosetests --verbosity=3 || die
}
