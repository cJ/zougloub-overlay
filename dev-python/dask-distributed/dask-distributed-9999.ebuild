# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Distributed computation in Python"
HOMEPAGE="https://distributed.readthedocs.io/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/dask/distributed.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE=""

BDEPEND=""
DEPEND="${PYTHON_DEPS}"

RDEPEND="
 ${PYTHON_DEPS}
 >=www-servers/tornado-4.2
 >=dev-python/toolz-0.7.4
 dev-python/msgpack
 >=dev-python/cloudpickle-0.2.1
 >=dev-python/dask-0.12.0
 >=dev-python/click-6.6
 dev-python/six
 dev-python/tblib
 dev-python/psutil
 dev-python/locket
 >=dev-python/zict-0.1.0
"

#DOCS=( README.rst )
