# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
DISTUTILS_USE_PEP517="setuptools"

inherit distutils-r1 git-r3

DESCRIPTION="Wrapper around the tesseract-ocr API for Optical Character Recognition (OCR)"
HOMEPAGE="https://github.com/sirfz/tesserocr"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

EGIT_REPO_URI="https://github.com/sirfz/tesserocr"

RDEPEND="
 dev-python/pillow[${PYTHON_USEDEP}]
 app-text/tesseract
"
DEPEND="
 $RDEPEND
"
BDEPEND="
 dev-python/cython[${PYTHON_USEDEP}]
"
