EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
DISTUTILS_SRC_TEST="nosetests"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1


DESCRIPTION="RDFLib is a Python library for working with RDF, a simple yet powerful language for representing information."
HOMEPAGE="https://github.com/RDFLib/rdflib"
SRC_URI="https://pypi.python.org/packages/source/r/rdflib/${P}.tar.gz"

LICENSE="BSD"
KEYWORDS="~x86"
SLOT="0"
IUSE="doc examples"

RDEPEND="
 ${PYTHON_DEPS}
 <=dev-python/pyparsing-1.5.7[${PYTHON_USEDEP}]
 dev-python/html5lib[${PYTHON_USEDEP}]
 dev-python/isodate[${PYTHON_USEDEP}]
 dev-python/sparqlwrapper[${PYTHON_USEDEP}]
"
DEPEND="${RDEPEND}"

python_install_all() {
	distutils-r1_python_install_all
	if use doc; then
		dodoc -r docs
	fi
	if use examples; then
		insinto /usr/share/doc/"${PF}"/
		doins -r examples
	fi
}
