# SPDX-FileCopyrightText: 2016-2023 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi

DESCRIPTION="Python bindings to picosat, a SAT solver"
HOMEPAGE="http://pypi.python.org/pypi/pycosat"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""

RDEPEND="
 ${PYTHON_DEPS}
"
DEPEND="
 $RDEPEND
"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	rm ${S}/test_pycosat.py
	local DOCS=( README.rst )
	distutils-r1_python_install_all
}
