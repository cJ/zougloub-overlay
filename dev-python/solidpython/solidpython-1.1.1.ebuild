EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
inherit distutils-r1
DISTUTILS_USE_SETUPTOOLS=pyproject.toml

DESCRIPTION="OpenSCAD wrapper, with a better API"
HOMEPAGE="https://github.com/SolidCode/SolidPython"
SRC_URI="https://pypi.python.org/packages/source/s/solidpython/${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ppc ppc64 sparc x86 ~x86-fbsd"
IUSE="test"

DEPEND=""
RDEPEND="
 media-gfx/openscad
 dev-python/euclid3[${PYTHON_USEDEP}]
"

DOCS=( AUTHORS README TODO )

