EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
DISTUTILS_USE_PEP517=poetry

inherit distutils-r1 git-r3

DESCRIPTION="OpenSCAD wrapper, with a better API"
HOMEPAGE="https://github.com/SolidCode/SolidPython"
SRC_URI=""
EGIT_REPO_URI="https://github.com/SolidCode/SolidPython.git"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ppc ppc64 sparc x86 ~x86-fbsd"
IUSE=""

DEPEND=""
RDEPEND="
 media-gfx/openscad
 dev-python/euclid3[${PYTHON_USEDEP}]
"

DOCS=( README.rst )

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}

