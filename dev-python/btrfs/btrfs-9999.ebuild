# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

EGIT_REPO_URI="https://github.com/knorrie/python-btrfs"

DESCRIPTION="Python Btrfs module"
HOMEPAGE="https://github.com/knorrie/python-btrfs"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

BDEPEND=""
RDEPEND="${PYTHON_DEPS}"
DEPEND="${PYTHON_DEPS}"
