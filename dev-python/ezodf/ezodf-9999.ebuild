# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
DISTUTILS_USE_PEP517=setuptools

inherit distutils-r1 git-r3

EGIT_REPO_URI="https://github.com/T0ha/ezodf"

DESCRIPTION="Create new or open existing OpenDocument (ODF) documents"
HOMEPAGE="https://github.com/T0ha/ezodf"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="test"

DEPEND=""
RDEPEND="${DEPEND}
	dev-python/lxml[${PYTHON_USEDEP}]
"

BDEPEND="
	test? ( dev-python/nose[${PYTHON_USEDEP}] )"
