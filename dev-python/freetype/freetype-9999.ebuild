# SPDX-FileCopyrightText: 2016-2022 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Freetype python bindings"
HOMEPAGE="https://github.com/rougier/freetype-py"
SRC_URI=""
EGIT_REPO_URI="https://github.com/rougier/freetype-py"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""
IUSE="test"

DEPEND="media-libs/freetype"

DOCS=( README.rst )

