# SPDX-FileCopyrightText: 2017-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi

DESCRIPTION="Python implementation of Ditz (http://rubygems.org/gems/ditz)."
HOMEPAGE="http://pythonhosted.org/pyditz/"

LICENSE="LGPL-2+"
SLOT="0"
IUSE=""

DEPEND="
"
RDEPEND="
 dev-python/jinja
 dev-python/six
 dev-python/pyyaml
 dev-python/cerberus
"

if [ "$PV" == "9999" ]; then
	inherit mercurial
	EHG_REPO_URI="https://bitbucket.org/zondo/pyditz"
	SRC_URI=""
	KEYWORDS=""
else
	S="${WORKDIR}/pyditz-${PV}"
	KEYWORDS="~amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
fi
