EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Kalman filtering and optimal estimation library in Python"
HOMEPAGE="https://github.com/rlabbe/filterpy"
SRC_URI=""
EGIT_REPO_URI="https://github.com/rlabbe/filterpy"

LICENSE="MIT"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ppc ppc64 sparc x86 ~x86-fbsd"
IUSE="test"

DEPEND="
 dev-python/numpy
 sci-libs/scipy
 dev-python/matplotlib
"

