# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Some convenient utilities not included with the standard Python install"
HOMEPAGE="https://github.com/WoLpH/python-utils/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/WoLpH/python-utils"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE="test"

RDEPEND="
 dev-python/six
"

DOCS=( README.rst )
