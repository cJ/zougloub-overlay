# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit git-r3 distutils-r1

DESCRIPTION="Distributed self-replicating programs in Python"
HOMEPAGE="https://mitogen.networkgenomics.com/"
SRC_URI=""

EGIT_REPO_URI="https://github.com/dw/mitogen"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""

RDEPEND="
"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}

