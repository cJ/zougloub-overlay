# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 virtualx pypi

DESCRIPTION="Python tools to manipulate graphs and complex networks"
HOMEPAGE="http://networkx.github.io/ https://github.com/networkx/networkx"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86 ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos"
IUSE="examples extras pandas scipy test xml yaml"
RESTRICT="!test? ( test )"

REQUIRED_USE="
	test? ( extras pandas scipy xml yaml )"

COMMON_DEPEND="
	extras? (
		>=dev-python/pygraphviz-1.5[${PYTHON_USEDEP}]
		>=sci-libs/gdal-1.10.0[python,${PYTHON_USEDEP}]
	)
	scipy? ( >=sci-libs/scipy-1.1.0[${PYTHON_USEDEP}] )
	xml? ( >=dev-python/lxml-4.2.3[${PYTHON_USEDEP}] )
	yaml? ( >=dev-python/pyyaml-3.13[${PYTHON_USEDEP}] )"
DEPEND="
	>=dev-python/decorator-4.3.0[${PYTHON_USEDEP}]
	${COMMON_DEPEND}
	test? (
		>=dev-python/matplotlib-2.2.2[${PYTHON_USEDEP}]
		dev-python/nose[${PYTHON_USEDEP}]
	)"
RDEPEND="
	>=dev-python/decorator-4.3.0[${PYTHON_USEDEP}]
	${COMMON_DEPEND}
	examples? (
		dev-python/pyparsing[${PYTHON_USEDEP}]
	)"

PATCHES=(
)

python_test() {
	virtx nosetests -vv
}

python_install_all() {
	use examples && dodoc -r examples

	distutils-r1_python_install_all
}
