# SPDX-FileCopyrightText: 2016-2022 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Pure Python package for reading and parsing libpcap savefiles."
HOMEPAGE="https://pypi.python.org/pypi/pypcapfile"
SRC_URI=""
EGIT_REPO_URI="https://github.com/kisom/pypcapfile.git"
LICENSE="ISC"
SLOT="0"
IUSE=""
KEYWORDS=""

RDEPEND="
 ${PYTHON_DEPS}
"
DEPEND="
 $RDEPEND
"

src_prepare() {
	distutils-r1_src_prepare
}

src_compile() {
	distutils-r1_src_compile
	#$(use threads || echo --without-threading)
}

src_test() {
	testing() {
		:
	}
	python_execute_function testing
}

src_install() {
	distutils-r1_src_install
}

pkg_setup() {
	python-any-r1_pkg_setup
}

