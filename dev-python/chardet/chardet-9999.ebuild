EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Character encoding auto-detection in Python."

HOMEPAGE="https://github.com/chardet/chardet"
SRC_URI=""
EGIT_REPO_URI="https://github.com/chardet/chardet.git"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS=""
IUSE=""

HTML_DOCS=( docs/ )

python_compile() {
	distutils-r1_python_compile
}

python_install() {
	distutils-r1_python_install
}

python_install_all() {
	local DOCS=( README.rst )
	local HTML_DOCS=( docs )
	distutils-r1_python_install_all
}

