# SPDX-FileCopyrightText: 2021-2023 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi


DESCRIPTION="Structured Logging for Python"
HOMEPAGE="http://www.structlog.org/"

EGIT_REPO_URI="https://github.com/hynek/structlog"

LICENSE="|| ( Apache-2.0 MIT )"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE="doc"

RDEPEND="dev-python/six[${PYTHON_USEDEP}]"
DEPEND="
 ${RDEPEND}
"

DOCS=docs

