# SPDX-FileCopyrightText: 2009-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi

DESCRIPTION="Simple bencode parser"
HOMEPAGE="https://github.com/fuzeman/bencode.py"
PN_="bencode.py"
S="${WORKDIR}/${PN_}-${PV}"

RESTRICT="primaryuri"
LICENSE="Bittorrent-1.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	local DOCS=( README.rst )
	distutils-r1_python_install_all
}

