EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
DISTUTILS_USE_PEP517=setuptools

inherit distutils-r1 git-r3

DESCRIPTION="libqrencode python ctypes bindings"
HOMEPAGE="https://github.com/zaazbb/libqrencode-ctypes"
SRC_URI=""

LICENSE="LGPL-3"
SLOT="0"
IUSE=""
KEYWORDS=""

EGIT_REPO_URI="https://github.com/zaazbb/libqrencode-ctypes"

RDEPEND="
 media-gfx/qrencode
"

DEPEND="
 $RDEPEND
"

src_prepare() {
	sed -i -e 's@/usr/local/lib/libqrencode.so@libqrencode.so@g' \
	 "${S}/libqrencode/_qrencode.py"
	distutils-r1_src_prepare
}
