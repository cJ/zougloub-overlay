# SPDX-FileCopyrightText: 2016-2023 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..12}
 pypy3
)

inherit distutils-r1 pypi

PYPI_PN="pdf417-py"

DESCRIPTION="PDF417 barcode generator for Python"
HOMEPAGE="https://github.com/ihabunek/pdf417-py"

if [ "$PV" == "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/ihabunek/pdf417-py"
	SRC_URI=""
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}

