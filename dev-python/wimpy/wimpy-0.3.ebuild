# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1

MY_COMMIT="03913afbc6133a579cc305132b8dd6cfb896f5f9"
DESCRIPTION="Anti-copy-pasta"
HOMEPAGE="https://github.com/wimglenn/wimpy"
SRC_URI="https://github.com/wimglenn/wimpy/archive/${MY_COMMIT}.tar.gz -> wimpy-v0.3-${MY_COMMIT}.tar.gz"

LICENSE="MIT"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/wimpy-${MY_COMMIT}"

