# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
)

inherit distutils-r1

DESCRIPTION="Provides a USBTMC driver for controlling instruments over USB"
HOMEPAGE="https://github.com/python-ivi/python-usbtmc"
SRC_URI="https://github.com/python-ivi/python-usbtmc/archive/v0.8.zip"

LICENSE="MIT"
KEYWORDS="~amd64"
SLOT="0"
IUSE=""

# As of 2023-01-20 pyusb doesn't support python3_11 or pypy3
RDEPEND="
 dev-python/pyusb[${PYTHON_USEDEP}]
"
DEPEND="
 $RDEPEND
"

S="${WORKDIR}/python-${P}"
