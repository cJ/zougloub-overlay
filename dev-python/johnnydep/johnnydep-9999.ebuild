# SPDX-FileCopyrightText: 2009-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 #pypy3 # TODO structlog
)

inherit distutils-r1 git-r3

DESCRIPTION="Display dependency tree of Python distribution"
HOMEPAGE="https://github.com/wimglenn/johnnydep"
SRC_URI=""

EGIT_REPO_URI="$HOMEPAGE"

LICENSE="MIT"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

DEPEND="
"

RDEPEND="
 dev-python/anytree[${PYTHON_USEDEP}]
 dev-python/cachetools[${PYTHON_USEDEP}]
 dev-python/colorama[${PYTHON_USEDEP}]
 dev-python/distlib[${PYTHON_USEDEP}]
 dev-python/oyaml[${PYTHON_USEDEP}]
 >=dev-python/packaging-17[${PYTHON_USEDEP}]
 dev-python/pip[${PYTHON_USEDEP}]
 dev-python/pkginfo[${PYTHON_USEDEP}]
 dev-python/structlog[${PYTHON_USEDEP}]
 dev-python/tabulate[${PYTHON_USEDEP}]
 dev-python/toml[${PYTHON_USEDEP}]
 >=dev-python/wheel-0.32.0[${PYTHON_USEDEP}]
 dev-python/wimpy[${PYTHON_USEDEP}]
"
