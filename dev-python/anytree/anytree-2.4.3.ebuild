# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)
PYTHON_REQ_USE="threads(+)"

inherit distutils-r1 pypi

DESCRIPTION="Powerful and Lightweight Python Tree Data Structure with various plugins."
HOMEPAGE="https://github.com/c0fec0de/anytree/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

# Tests only work with the GitPython repo
RESTRICT="test"

RDEPEND="
"
DEPEND="
 ${RDEPEND}
 test? (
  dev-python/nose[${PYTHON_USEDEP}]
 )
"
