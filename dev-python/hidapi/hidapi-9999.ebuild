# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="A Cython interface to HIDAPI library"
HOMEPAGE="https://github.com/trezor/cython-hidapi"
SRC_URI=""

EGIT_REPO_URI="https://github.com/trezor/cython-hidapi"

LICENSE="|| ( BSD GPL-3 )"
# and more

SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86 ~amd64-linux ~x86-linux ~ppc-macos"
IUSE=""

DEPEND="
 >=dev-python/cython-0.24[${PYTHON_USEDEP}]
"
RDEPEND="
"

# --with-system-hidapi
