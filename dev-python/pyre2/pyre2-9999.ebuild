# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit git-r3 distutils-r1 pypi

DESCRIPTION="Python extension that wraps Google's RE2 library."
HOMEPAGE="https://github.com/axiak/pyre2 http://pypi.python.org/pypi/re2/"

if [ "$PV" == "9999" ]; then
   EGIT_REPO_URI="https://github.com/facebook/pyre2.git"
   SRC_URI=""
fi

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND="
 dev-libs/re2
 dev-python/cython
"
RDEPEND="
 dev-libs/re2
"

if [ "$PV" == "9999" ]; then
	:
else
	S="${WORKDIR}/re2-${PV}"
fi

python_prepare_all() {
	rm -f README
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}

