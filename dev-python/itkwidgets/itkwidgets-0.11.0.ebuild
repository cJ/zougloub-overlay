# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Ebuild generated by g-pypi 0.4

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1


DESCRIPTION="Jupyter widgets to visualize images in 2D and 3D"
HOMEPAGE="https://github.com/InsightSoftwareConsortium/itk-jupyter-widgets"
SRC_URI="https://files.pythonhosted.org/packages/f4/d9/d7d917eb71ae867cded7bc72e7ce6cb586bfacfa42dd298b73da89fea2dd/${P}.tar.gz"

LICENSE=""
KEYWORDS="~amd64"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="
 >=dev-python/ipywidgets-7.1.2[${PYTHON_USEDEP}]
 sci-libs/itk
 dev-python/numpy[${PYTHON_USEDEP}]
 dev-python/six[${PYTHON_USEDEP}]
 dev-python/zstandard[${PYTHON_USEDEP}]
"

#sci-libs/itk[python]



