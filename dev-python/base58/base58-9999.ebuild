# SPDX-FileCopyrightText: 2009-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi

DESCRIPTION="Library and CLI for base58 as used by the bitcoin network."
HOMEPAGE="https://github.com/keis/base58"

LICENSE="MIT"
SLOT="0"
IUSE="doc"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

BDEPEND=""
RDEPEND="${PYTHON_DEPS}"
DEPEND="${PYTHON_DEPS}"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/keis/base58.git"
	EGIT_BRANCH="master"
	SRC_URI=""
	KEYWORDS=""
else
	KEYWORDS="~amd64 ~x86"
fi
