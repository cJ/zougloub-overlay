EAPI=8

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="ctypes binding to zbar."
HOMEPAGE="https://github.com/NaturalHistoryMuseum/pyzbar"
SRC_URI=""
EGIT_REPO_URI="https://github.com/NaturalHistoryMuseum/pyzbar"
LICENSE="MIT"
SLOT="0"
IUSE=""
KEYWORDS=""

RDEPEND="
"

BDEPEND="
 ${RDEPEND}
"
