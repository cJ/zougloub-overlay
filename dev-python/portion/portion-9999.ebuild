# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="data structure and operations for intervals"
HOMEPAGE="https://github.com/AlexandreDecan/portion"
SRC_URI=""

EGIT_REPO_URI="https://github.com/AlexandreDecan/portion"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
 ${PYTHON_DEPS}
"
DEPEND="
 $RDEPEND
"
BDEPEND="
"
