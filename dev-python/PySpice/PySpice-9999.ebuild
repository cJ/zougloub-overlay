# Copyright 2021-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..13}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Simulate electronic circuit using Python and the Ngspice / Xyce simulators"
HOMEPAGE="https://pyspice.fabrice-salvaire.fr/"
SRC_URI=""

EGIT_REPO_URI="https://github.com/PySpice-org/PySpice"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86 ~amd64-linux ~x86-linux ~ppc-macos"
IUSE=""

DEPEND="
"
RDEPEND="
"
