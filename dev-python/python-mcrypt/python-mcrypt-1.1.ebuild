# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 pypi

DESCRIPTION="Python interface to mcrypt library"
HOMEPAGE="http://labix.org/python-mcrypt http://pypi.python.org/pypi/python-mcrypt"
SRC_URI="http://labix.org/download/${PN}/${P}.tar.gz"
LICENSE="LGPL"
SLOT="0"
KEYWORDS="~amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND="
 >=dev-libs/libmcrypt-2.5.0:0
"

RDEPEND="
 >=dev-libs/libmcrypt-2.5.0:0
"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_install_all() {
	distutils-r1_python_install_all
}

