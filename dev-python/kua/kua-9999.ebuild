# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION=" Lightning fast URL routing in Python (trie router)"
HOMEPAGE="http://kua.readthedocs.io"
SRC_URI=""
EGIT_REPO_URI="https://github.com/nitely/kua.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm64 x86 ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos"
IUSE=""

REQUIRED_USE=""

COMMON_DEPEND=""
DEPEND="
"
RDEPEND="
"

PATCHES=(
)

python_install_all() {
	distutils-r1_python_install_all
}
