# SPDX-FileCopyrightText: 2016-2022 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Simple library to make working with STL files (and 3D objects in general) fast and easy"
HOMEPAGE="https://github.com/WoLpH/numpy-stl/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/WoLpH/numpy-stl"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE="test"

RDEPEND="
 dev-python/numpy[${PYTHON_USEDEP}]
 dev-python/python-utils[${PYTHON_USEDEP}]
"
DEPEND+="
 dev-python/numpy[${PYTHON_USEDEP}]
"

BDEPEND+="
 dev-python/cython[${PYTHON_USEDEP}]
"

DOCS=( README.rst )


src_prepare() {
	default
	rm -rf tests
}
