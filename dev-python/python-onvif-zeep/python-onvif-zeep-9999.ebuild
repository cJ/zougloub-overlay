EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="ONVIF Client Implementation"
HOMEPAGE="https://github.com/FalkTannhaeuser/python-onvif-zeep"
EGIT_REPO_URI="https://github.com/FalkTannhaeuser/python-onvif-zeep"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-python/zeep-2.0.0"


