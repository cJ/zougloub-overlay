# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Tbrary to parse, compare, simplify and normalize license expressions"
HOMEPAGE="https://github.com/nexB/license-expression"
SRC_URI=""

EGIT_REPO_URI="https://github.com/nexB/license-expression"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
"
RDEPEND="
 >=dev-python/boolean-py-3.8[${PYTHON_USEDEP}]
"

