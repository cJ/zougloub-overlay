# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1

DESCRIPTION="Lightweight, extensible schema and data validation tool for Python dictionaries."
HOMEPAGE="http://python-cerberus.org"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""

BDEPEND=""
RDEPEND="${PYTHON_DEPS}"
DEPEND="${PYTHON_DEPS}"

if [ "$PV" == "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/nicolaiarocci/cerberus"
	SRC_URI=""
else
	S="${WORKDIR}/cerberus-${PV}"
fi

