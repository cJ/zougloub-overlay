# SPDX-FileCopyrightText: 2016-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

DISTUTILS_USE_PEP517="setuptools"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Ceres solver bindings for python"
HOMEPAGE="https://github.com/rll/cyres"
SRC_URI=""
EGIT_REPO_URI="https://github.com/exmakhina/cyres"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""
IUSE="test"

BDEPEND=""
RDEPEND="${PYTHON_DEPS} sci-libs/ceres-solver"
DEPEND="${PYTHON_DEPS} sci-libs/ceres-solver"

DOCS=( README.md )

# TODO pxd files present as well as cyresc

