EAPI="8"

inherit git-r3 cmake

HOMEPAGE="https://github.com/fontmatrix/fontmatrix"
LICENSE="BSD"
EGIT_REPO_URI="https://github.com/fontmatrix/fontmatrix"
DESCRIPTION="Font management application"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
 dev-qt/qtcore:5
 dev-qt/qtsvg:5
 dev-qt/qtsql:5
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	 #-DWANT_HARFBUZZ=ON
	 #-DWANT_ICU=ON
	 #-DWANT_M17N=ON
	 #-DWANT_PODOFO=ON
	)
	cmake_src_configure
}
