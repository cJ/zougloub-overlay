# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

inherit git-r3

HOMEPAGE="https://gitlab.freedesktop.org/xdg/xdgmime"
DESCRIPTION="Module using the xdg shared mime database"
LICENSE="LGPL-2.1 AFL-2.1"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

EGIT_REPO_URI="https://gitlab.freedesktop.org/xdg/xdgmime"

RDEPEND="
"

DEPEND="${RDEPEND}
"

src_compile() {
	#default
	${CC} -shared -o libxdgmime.so src/xdg*.c -fPIC -Wall -Wmissing-prototypes  -Wno-sign-compare -g -DXDG_PREFIX=xdg_mime -DHAVE_MMAP
}

src_install() {
	insinto /usr/include/xdgmime
	doins src/*.h
	dolib.so libxdgmime.so
}
