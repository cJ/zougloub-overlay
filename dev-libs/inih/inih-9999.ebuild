# Copyright 2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit meson git-r3

DESCRIPTION="Simple .INI file parser in C, good for embedded systems"
HOMEPAGE="https://github.com/benhoyt/inih"

EGIT_REPO_URI="https://github.com/benhoyt/inih.git"

if [ ${PV} = "9999" ]; then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="BSD"
SLOT="0"

if [[ "${PV}" == "9999" ]]; then
	KEYWORDS=""
else
	KEYWORDS="~amd64 ~x86"
fi

DEPEND="
"

RDEPEND="
"

src_configure() {
	local emesonargs=(
	 -D distro_install=true
	 -D with_INIReader=true
	)
	meson_src_configure
}
