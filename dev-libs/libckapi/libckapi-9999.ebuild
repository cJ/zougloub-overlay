# Copyright (c) 2021 Jérôme Carretero
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit git-r3 autotools

DESCRIPTION="Userspace interface for Linux kernel crypto API"
HOMEPAGE="https://github.com/smuellerDD/libkcapi"
SRC_URI=""
SLOT="0"

EGIT_REPO_URI="https://github.com/smuellerDD/libkcapi"

LICENSE="|| ( GPL-2 BSD )"
KEYWORDS="amd64 ~arm arm64 hppa ppc ppc64 sparc x86"
IUSE=""

DEPEND=""

src_prepare() {
	default
	[[ "${PV}" == *9999 ]] && eautoreconf
}

src_configure() {
	local myeconfargs=(
	 --enable-kcapi-test \
	 --enable-kcapi-speed \
	 --disable-kcapi-hasher \
	 --enable-kcapi-rngapp \
	 --enable-kcapi-encapp \
	 --enable-kcapi-dgstapp \
	 )
	 econf "${myeconfargs[@]}"
}

src_install() {
	default
}
