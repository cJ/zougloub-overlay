
EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit cmake git-r3 multilib python-r1

DESCRIPTION="A minimal library for dynamically-loaded or statically-linked functionality modules."
HOMEPAGE="https://github.com/OSVR/libfunctionality"
EGIT_REPO_URI="https://github.com/OSVR/libfunctionality"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
"
DEPEND="
"
