# Copyright 2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit cmake git-r3

DESCRIPTION="Lightweight C++ command line option parser"
HOMEPAGE="https://github.com/jarro2783/cxxopts"

EGIT_REPO_URI="https://github.com/jarro2783/cxxopts.git"

if [ ${PV} = "9999" ]; then
	:
else
	# e6858d3429e0ba5fe6f42ce2018069ce992f3d26
	EGIT_COMMIT="v${PV}"
fi

LICENSE="MIT"
SLOT="0"

if [[ "${PV}" == "9999" ]]; then
	KEYWORDS="~amd64 ~x86"
else
	KEYWORDS="~amd64 ~x86"
fi

DEPEND="
"

RDEPEND="
"

