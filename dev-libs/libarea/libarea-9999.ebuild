EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/Heeks/libarea"
DESCRIPTION="Library and python module for pocketing and profiling operations"
HOMEPAGE="https://github.com/Heeks/libarea"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE="+python"
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
"

DEPEND="${RDEPEND}
"
