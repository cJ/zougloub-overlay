# Copyright 2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit cmake git-r3

DESCRIPTION="Cross-platform logging made easier for C++ applications"
HOMEPAGE="https://github.com/amrayn/easyloggingpp"

EGIT_REPO_URI="https://github.com/amrayn/easyloggingpp.git"

if [ ${PV} = "9999" ]; then
	:
	EGIT_COMMIT="v${PV}"
fi

LICENSE="MIT"
SLOT="0"

if [[ "${PV}" == "9999" ]]; then
	KEYWORDS="~amd64 ~x86"
else
	KEYWORDS="~amd64 ~x86"
fi

DEPEND="
"

RDEPEND="
"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
	 -D build_static_lib=ON
	 -D lib_utc_datetime=ON
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install
}

