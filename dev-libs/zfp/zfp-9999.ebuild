EAPI="8"

EGIT_REPO_URI="https://github.com/LLNL/zfp"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
)

inherit multilib cmake git-r3 python-r1

DESCRIPTION="Compressed format for representing multidimensional floating-point and integer arrays"
HOMEPAGE="https://github.com/LLNL/zfp"
SRC_URI=""

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE=""

CDEPEND="
"
DEPEND="
 ${CDEPEND}
"
RDEPEND="
 ${CDEPEND}
"
BDEPEND="
 dev-python/cython[${PYTHON_USEDEP}]
"

IUSE="+python"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

src_configure() {
	local mycmakeargs=(
	)
	cmake_src_configure
}

src_configure() {

	my_configure() {
		local mycmakeargs=(
		)

		if use python; then
			mycmakeargs+=(
				-D BUILD_ZFPY:BOOL=1
			)
		fi

		cmake_src_configure
	}

	if use python; then
		python_foreach_impl my_configure
	else
		my_configure
	fi
}


src_compile() {
	if use python; then
		python_foreach_impl cmake_src_compile
	else
		cmake_src_compile
	fi
}


src_install() {
	my_install() {
		cmake_src_install
		python_optimize
	}

	if use python; then
		python_foreach_impl my_install
	else
		cmake_src_install
	fi
}
