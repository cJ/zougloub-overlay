# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 #pypy3
)

inherit git-r3 cmake python-r1 toolchain-funcs

EGIT_REPO_URI="https://github.com/aewallin/opencamlib"
DESCRIPTION="c++ library for creating toolpaths for cnc-machines such as mills and lathes"
HOMEPAGE="https://github.com/aewallin/opencamlib"
LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
IUSE="+python +openmp"
RESTRICT="primaryuri"
SLOT="0"

# TODO python

RDEPEND="
 python? (
  ${PYTHON_DEPS}
 )
"

DEPEND="${RDEPEND}
"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"


pkg_setup() {
	use python && python-r1_pkg_setup
	use openmp && tc-check-openmp
}


src_prepare() {
	cmake_src_prepare

	if use python; then
		: #python_foreach_impl prepare_python_bindings
	fi
}


src_configure() {

	opencamlib_configure() {
		local mycmakeargs=(
			-DBUILD_CXX_LIB=ON
			-DBUILD_DOC=OFF # doesn't work
			-DBUILD_EMSCRIPTEN_LIB=OFF # needs nodejs
			-DBUILD_NODEJS_LIB=OFF	# net-libs/nodejs currently support only python-2.7
			-DUSE_OPENMP=$(usex openmp)
		)

		if use python; then
			python_export PYTHON_INCLUDEDIR PYTHON_LIBPATH
			mycmakeargs+=(
				-DBUILD_PY_LIB=ON
				-DPython3_EXECUTABLE="${PYTHON}"
				-DPython3_LIBRARY="${PYTHON_LIBPATH}"
				-DPython3_INCLUDE_DIR="${PYTHON_INCLUDEDIR}"
				-DUSE_PY_3=ON
				-DBoost_USE_STATIC_LIBS=OFF
			)
		fi

		cmake_src_configure
	}

	if use python; then
		python_foreach_impl opencamlib_configure
	else
		opencamlib_configure
	fi
}


src_compile() {
	if use python; then
		python_foreach_impl cmake_src_compile
	else
		cmake_src_compile
	fi
}


src_install() {

	opencamlib_install() {
		cmake_src_install
		python_optimize
	}

	if use python; then
		python_foreach_impl opencamlib_install
	else
		cmake_src_install
	fi
}
