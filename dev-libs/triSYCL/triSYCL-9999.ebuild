# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake git-r3

DESCRIPTION="Implementation of OpenCL SYCL"
HOMEPAGE="https://github.com/triSYCL/triSYCL"
SRC_URI=""
EGIT_REPO_URI="https://github.com/triSYCL/triSYCL"

RESTRICT="primaryuri"
LICENSE="LLVM"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="+opencl +openmp"

# cuda/opencl loaded dynamically at runtime, no compile time dep
RDEPEND="
 opencl? ( virtual/opencl )
"
DEPEND="
"
#S="${WORKDIR}/${PN}-${MYPV}"

src_prepare() {
	sed -i -e 's/add_subdirectory.tests.//g' CMakeLists.txt
}

src_configure() {
	local mycmakeargs=(
	 -DTRISYCL_OPENMP=$(usex openmp)
	 -DTRISYCL_OPENCL=$(usex opencl)
	)
	cmake_src_configure
}

src_install() {
	insinto /usr
	doins -r include
}
