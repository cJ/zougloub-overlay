EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/KhronosGroup/OpenXR-SDK"
DESCRIPTION="OpenXR headers and loader"
HOMEPAGE="https://github.com/KhronosGroup/OpenXR-SDK"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
 media-libs/mesa
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	)
	cmake_src_configure
}
