EAPI="8"

EGIT_REPO_URI="https://github.com/DynamoRIO/dynamorio"

inherit toolchain-funcs multilib cmake git-r3

DESCRIPTION="Dynamic Instrumentation Tool Platform"
HOMEPAGE="https://github.com/DynamoRIO/dynamorio"
SRC_URI=""

LICENSE="MPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+python"

CDEPEND="
"
DEPEND="
 ${CDEPEND}
"
RDEPEND="
 ${CDEPEND}
"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

src_configure() {
	local mycmakeargs=(
	 -DCMAKE_BUILD_TYPE:STRING="Release"
	)

	tc-export CC CXX
	cmake_src_configure
}

src_compile() {
	cmake_src_make
}

src_install() {
	cmake_src_install
}

