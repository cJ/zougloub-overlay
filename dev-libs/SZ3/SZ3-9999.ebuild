EAPI="8"

EGIT_REPO_URI="https://github.com/szcompressor/SZ3"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit multilib cmake git-r3 python-r1

DESCRIPTION="Modular Error-bounded Lossy Compression Framework for Scientific Datasets"
HOMEPAGE="https://szcompressor.org/"
SRC_URI=""

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE=""

CDEPEND="
"
DEPEND="
 ${CDEPEND}
"
RDEPEND="
 ${CDEPEND}
"

IUSE="+python"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

src_install() {
	cmake_src_install
	default
	python_foreach_impl python_domodule tools/pysz/pysz.py
}
