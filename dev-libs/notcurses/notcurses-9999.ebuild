EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/dankamongmen/notcurses"
DESCRIPTION="Library and python module for pocketing and profiling operations"
HOMEPAGE="https://nick-black.com/dankwiki/index.php/Notcurses"
LICENSE="Apache-2.0"
KEYWORDS="~amd64 ~x86"
IUSE="" #python"
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	 -DUSE_TESTS=OFF
	 -DUSE_PANDOC=OFF
	 -DUSE_QRCODEGEN=OFF
	 -DUSE_PYTHON=OFF #$(cmake_use_build python PYTHON)
	)
	cmake_src_configure
}

