EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/mkeeter/fstl"
DESCRIPTION="STL file viewer"
HOMEPAGE="https://github.com/mkeeter/fstl"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
 >=dev-qt/qtopengl-5.0
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	)
	cmake_src_configure
}
