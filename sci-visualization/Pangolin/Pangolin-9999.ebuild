EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/stevenlovegrove/Pangolin.git"
DESCRIPTION="Library for managing OpenGL display / interaction and abstracting video input"
HOMEPAGE="https://github.com/stevenlovegrove/Pangolin.git"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE="uvc png jpeg tiff openexr"
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
 media-libs/glew
 uvc? ( media-libs/libuvc )
 png? ( media-libs/libpng )
 openexr? ( media-libs/openexr )
 tiff? ( media-libs/tiff )
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	 -DLIB_INSTALL_DIR=/usr/lib64
	)
	cmake_src_configure
}
