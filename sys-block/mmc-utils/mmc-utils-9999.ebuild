# Copyright 2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit git-r3

DESCRIPTION="Tool for configuring MMC storage devices from userspace"
HOMEPAGE="https://git.kernel.org/pub/scm/utils/mmc/mmc-utils.git/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="
"

EGIT_REPO_URI="https://git.kernel.org/pub/scm/utils/mmc/mmc-utils.git"
EGIT_BRANCH="master"

