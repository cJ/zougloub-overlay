EAPI="8"

inherit git-r3 cmake

EGIT_REPO_URI="https://github.com/GammaUNC/PQP"
DESCRIPTION="A library for performing proximity queries on a pair of geometric models composed of triangles"
HOMEPAGE="http://gamma.cs.unc.edu/SSV/"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="primaryuri"
SLOT="0"

RDEPEND="
"

DEPEND="${RDEPEND}
"

src_configure() {
	local mycmakeargs=(
	)
	cmake_src_configure
}
