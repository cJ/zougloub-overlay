# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

inherit meson git-r3

REV=${PV#*_p}

DESCRIPTION="Opengl test suite"
HOMEPAGE="https://launchpad.net/glmark2"
SRC_URI=""
EGIT_REPO_URI="https://github.com/glmark2/glmark2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~arm ~amd64 ~x86"
IUSE="drm gles2 +opengl wayland X"

RDEPEND="
 media-libs/libpng
 media-libs/mesa[gles2?]
 X? ( x11-libs/libX11 )
 wayland? ( >=dev-libs/wayland-1.2 )
"
DEPEND="${RDEPEND}
 virtual/pkgconfig
"

REQUIRED_USE="
 || ( opengl gles2 )
 || ( drm wayland X )
"


src_configure() {
	local myconf

	if use X; then
		use opengl && myconf+="x11-gl"
		use gles2 && myconf+=",x11-glesv2"
	fi

	if use drm; then
		use opengl && myconf+=",drm-gl"
		use gles2 && myconf+=",drm-glesv2"
	fi

	if use wayland; then
		use opengl && myconf+=",wayland-gl"
		use gles2 && myconf+=",wayland-glesv2"

	fi
	myconf=${myconf#,}

	local emesonargs=(
	 -Dflavors=${myconf}
	)
	meson_src_configure
}
