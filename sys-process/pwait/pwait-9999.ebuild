# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="8"

inherit cmake git-r3

DESCRIPTION="A comfortable way of running batch jobs"
HOMEPAGE="https://github.com/diazona/pwait"
EGIT_REPO_URI="https://github.com/diazona/pwait"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_prepare() {
	sed -i -e '/^install.CODE/d' CMakeLists.txt
	cmake_src_prepare
}

src_install() {
	cmake_src_install
	setcap cap_sys_ptrace+ep ${DESTDIR}${bindir}/pwait
}
