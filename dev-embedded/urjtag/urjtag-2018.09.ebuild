# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="8"



DESCRIPTION="Tool for communicating over JTAG with flash chips, CPUs, and many more"
HOMEPAGE="http://urjtag.sourceforge.net/"
SRC_URI="mirror://sourceforge/urjtag/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ppc sparc x86"
IUSE="ftdi readline"

DEPEND="
 !dev-embedded/jtag
 ftdi? ( dev-embedded/libftdi:1 )
 readline? ( sys-libs/readline )
"

src_configure() {
	econf \
	 $(use_with ftdi libftdi) \
	 $(use_with readline) \
	 || die
}

src_compile() {
	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die "failed to install"
	dodoc AUTHORS ChangeLog NEWS README THANKS
}
