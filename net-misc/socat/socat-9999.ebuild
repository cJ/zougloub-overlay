# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit autotools flag-o-matic toolchain-funcs git-r3

DESCRIPTION="Multipurpose relay : netcat++ (extended design, new implementation)"
HOMEPAGE="https://www.dest-unreach.org/socat/"

EGIT_PROJECT="socat"
EGIT_REPO_URI="https://repo.or.cz/socat.git"

LICENSE="GPL-2"
KEYWORDS=""
SLOT="0"
IUSE="ipv6 readline ssl tcpd"

DEPEND="
 ssl? ( >=dev-libs/openssl-3:0= )
 readline? ( sys-libs/readline:= )
 tcpd? ( sys-apps/tcp-wrappers )
"

RDEPEND="
 ${DEPEND}
"

# Tests are a large bash script
# Hard to disable individual tests needing network or privileges
RESTRICT="
 primaryuri
 test
 ssl? ( readline? ( bindist ) )
"

DOCS=( BUGREPORTS CHANGES DEVELOPMENT EXAMPLES FAQ FILES PORTING README SECURITY )

pkg_setup() {
	# bug #587740
	if use readline && use ssl ; then
		elog "You are enabling both readline and openssl USE flags, the licenses"
		elog "for these packages conflict. You may not be able to legally"
		elog "redistribute the resulting binary."
	fi
}

src_prepare() {
	ewarn "You will need to read socat.yo because man and html are not generated in this version (would require a bunch of useless dependencies)"

	ebegin "Patching makefile to not build regular documentation..."
	perl -i.bak -p -e 's|^doc: doc/socat.1 doc/socat.html$|doc: doc/socat.yo|' Makefile.in
	perl -i.bak -p -e 's|^\t\$\(INSTALL\) -m 644 \$\(srcdir\)/doc/socat.1 \$\(DESTDIR\)\$\(MANDEST\)/man1/$||' Makefile.in
	perl -i.bak -p -e 's|^DOCFILES = (.*) doc/socat.1 doc/socat.html (.*)$|DOCFILES = $1 $2|' Makefile.in
	perl -i.bak -p -e 's|^install: progs \$\(srcdir\)/doc/socat.1|install: progs|' Makefile.in
	eend $?

	eautoreconf
	default
}

src_configure() {
	# bug #293324
	filter-flags -Wall '-Wno-error*'
	tc-export AR

	econf \
	 $(use_enable ssl openssl) \
	 $(use_enable readline) \
	 $(use_enable ipv6 ip6) \
	 $(use_enable tcpd libwrap)
}

src_install() {
	default

	docinto examples
	dodoc *.sh
	dodoc doc/socat.yo

	docinto html
	dodoc doc/*.html doc/*.css
}
