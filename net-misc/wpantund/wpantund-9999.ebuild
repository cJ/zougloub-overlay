EAPI="8"

inherit autotools git-r3

DESCRIPTION="wpantund, Userspace WPAN Network Daemon"
HOMEPAGE="https://github.com/openthread/wpantund.git"
SRC_URI=""
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~x86 ~amd64"
EGIT_REPO_URI="https://github.com/openthread/wpantund.git"

IUSE=""

RDEPEND="
 dev-libs/boost
 sys-libs/readline
 sys-apps/dbus
"
DEPEND="
 sys-devel/autoconf-archive
"

src_prepare() {
	eautoreconf
	default
}

