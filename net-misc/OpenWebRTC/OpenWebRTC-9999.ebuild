EAPI="5"

inherit git-r3 autotools

DESCRIPTION=""
KEYWORDS="amd64 x86"
IUSE=""
SRC_URI=""
EGIT_REPO_URI="https://github.com/EricssonResearch/OpenWebRTC"

RDEPEND="
 media-plugins/openwebrtc-gst-plugins
"
DEPEND="${RDEPEND}"

src_prepare() {
	./autogen.sh
}

