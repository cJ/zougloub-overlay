# SPDX-FileCopyrightText: 2009-2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"

PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit python-r1 git-r3

DESCRIPTION="Comedi DAQ library"
HOMEPAGE="http://www.comedi.org"
SRC_URI=""

EGIT_REPO_URI="https://github.com/Linux-Comedi/comedilib.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="-ruby +python doc"

DEPEND="
 python? ( ${PYTHON_DEPS} dev-lang/swig )
 ruby? ( dev-lang/ruby dev-lang/swig )
 doc? ( app-text/docbook2X )
"
RDEPEND="${DEPEND}"


src_prepare() {
	default
	./autogen.sh
	if use python; then
		python_copy_sources
	fi
}

src_configure() {
	if use python; then
		python_foreach_impl run_in_build_dir \
		 econf \
		 $(use_enable ruby ruby-binding) \
		 $(use_enable python python-binding) \
		 || die "Econf failed"
	else
		econf \
		 $(use_enable ruby ruby-binding) \
		 $(use_enable python python-binding) \
		 || die "Econf failed"
	fi
}

src_install() {
	if use python; then
		python_foreach_impl run_in_build_dir default
	else
		default
	fi
}
