
EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit cmake git-r3 multilib python-r1


DESCRIPTION="The core libraries, applications, and plugins of the OSVR software platform."
HOMEPAGE="https://github.com/OSVR/OSVR-Core"
EGIT_REPO_URI="https://github.com/OSVR/OSVR-Core"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

MY_CDEPEND="
 dev-libs/libfunctionality
"

RDEPEND="
 $MY_CDEPEND
"
DEPEND="
 $MY_CDEPEND
"

