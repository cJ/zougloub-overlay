

DESCRIPTION="Tralics: a LaTeX to XML translator"
HOMEPAGE="http://www-sop.inria.fr/apics/tralics/"
SRC_URI="ftp://ftp-sop.inria.fr/marelle/tralics/src/tralics-src-2.15.4.tar.gz"
RESTRICT="mirror"

LICENSE="CeCILL"
KEYWORDS="~amd64 ~ppc ~sparc ~x86"
IUSE=""

DEPEND="
	virtual/tex-base
	"
SLOT="0"

src_compile() {
	cd src
	emake
}


src_install() {
	dobin src/tralics
	dodoc README COPYING
	mkdir ${D}/usr/share/${PN}
	cp -R confdir styles xml ${D}/usr/share/${PN}/
}

src_test() {
	cd ${S}/test
	${D}/usr/bin/tralics *
}
