# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="Converts ANSI terminal sequences to HTML"
HOMEPAGE="https://gitorious.org/ansicat/ansicat"
SRC_URI=""
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/python-3.0"
DEPEND="
 $RDEPEND
"
RESTRICT="primaryuri"

EGIT_REPO_URI="https://github.com/zougloub/ansicat.git"

src_install() {
	dobin ansi*
}

