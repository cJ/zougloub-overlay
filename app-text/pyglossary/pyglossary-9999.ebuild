# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ-gentoo@zougloub.eu>
# SPDX-License-Identifier: MIT

EAPI="8"
PYTHON_COMPAT=(
 python2_7
 python3_{6..11}
 pypy3
)

inherit distutils-r1 git-r3

DESCRIPTION="Working on glossarys (dictionary databases) using python"
HOMEPAGE="https://github.com/ilius/pyglossary"
SRC_URI=""
EGIT_REPO_URI="https://github.com/ilius/pyglossary.git"
LICENSE="GPL-3+"
KEYWORDS="~x86 ~amd64"
SLOT="0"
IUSE="gtk tk"

RESTRICT="primaryuri"

RDEPEND="
 gtk? ( dev-python/pygobject x11-libs/gtk+:3 )
 tk? ( $(python_gen_impl_dep tk) )
"

src_install() {
	distutils-r1_src_install
}

