EAPI="5"

inherit git-r3 autotools

DESCRIPTION="H.265 encoder plugin for GStreamer."
KEYWORDS="amd64 x86"
IUSE=""
SRC_URI=""
EGIT_REPO_URI="https://github.com/EricssonResearch/openwebrtc-gst-plugins"

RDEPEND="
 net-libs/usrsctp
"
DEPEND="${RDEPEND}"

src_prepare() {
	eautoreconf
}

